package com.azertumc.towny.player;

import com.azertumc.towny.Towny;
import com.azertumc.towny.item.CItem;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

public class TownPlayer {

    private UUID uuid;
    private Player player;

    public TownPlayer(UUID uuid) {
        this.uuid=uuid;
        this.player= Bukkit.getPlayer(uuid);
    }

    public void sendMessage(String content) {
        player.sendMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "Kingdoms " + ChatColor.DARK_AQUA + ">> " + ChatColor.RESET + content);
    }

    public int getSetting(String name) {

        try {
            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM kingdom_set WHERE name=?;");
            get.setString(1,name);
            ResultSet rs = get.executeQuery();
            if (rs.next()) {
                return rs.getInt(name);
            }
        } catch(Exception e) {
        }

        return 0;
    }

    public void disband() {
        try {

            PreparedStatement del = Towny.getDbConnection().getConnection().prepareStatement("DELETE FROM kingdom_set WHERE name=?;");
            PreparedStatement del1 = Towny.getDbConnection().getConnection().prepareStatement("DELETE FROM chunk_data WHERE name=?;");

            del.setString(1,getKingdom());
            del1.setString(1,getKingdom());

            del.execute();
            del1.execute();


        } catch(Exception e) {
        }
        delete();
    }

    public void openSettings() {
        Inventory inventory = Bukkit.createInventory(null,27,getKingdom() + " - Settings");

        if (getSetting("pvp") == 0) {
            CItem pvp = new CItem(ChatColor.GRAY + "PVP - " + ChatColor.DARK_RED + ChatColor.BOLD + "✖",Material.WOOL,(byte)14);
            inventory.setItem(12,pvp.getItemStack());
        } else if (getSetting("pvp") == 1) {
            CItem pvp = new CItem(ChatColor.GRAY + "PVP - " + ChatColor.DARK_GREEN + ChatColor.BOLD + "✔",Material.WOOL,(byte)5);
            inventory.setItem(12,pvp.getItemStack());
        }


        if (getSetting("build") == 0) {
            CItem pvp = new CItem(ChatColor.GRAY + "Build - " + ChatColor.DARK_RED + ChatColor.BOLD + "✖",Material.WOOL,(byte)14);
            inventory.setItem(14,pvp.getItemStack());
        } else if (getSetting("build") == 1) {
            CItem pvp = new CItem(ChatColor.GRAY + "Build - " + ChatColor.DARK_GREEN + ChatColor.BOLD + "✔",Material.WOOL,(byte)5);
            inventory.setItem(14,pvp.getItemStack());
        }

        getPlayer().openInventory(inventory);
    }

    public void delete() {
        if (getKingdom().equalsIgnoreCase("none")) {
            return;
        }

        try {
            PreparedStatement del = Towny.getDbConnection().getConnection().prepareStatement("DELETE FROM kingdom_player WHERE uuid=?;");
            del.setString(1,uuid.toString());
            del.execute();

        } catch(Exception e) {
        }
    }

    public void assignKingdom(String s) {
        try {
            if (getKingdom().equalsIgnoreCase("None")) {
                PreparedStatement up = Towny.getDbConnection().getConnection().prepareStatement("INSERT INTO kingdom_player values(?,?,?);");
                up.setString(1,uuid.toString());
                up.setString(2,s);
                up.setInt(3,5);
                up.execute();
            } else {
                PreparedStatement up = Towny.getDbConnection().getConnection().prepareStatement("UPDATE kingdom_player SET kingdom=? WHERE uuid=?;");
                up.setString(1,s);
                up.setString(2,uuid.toString());
                up.executeUpdate();
                updateRank(0);
            }
        } catch(Exception e) {
        }
    }

    public void updateRank(int i) {
        if (getKingdom().equalsIgnoreCase("None")) return;
        try {
            PreparedStatement up = Towny.getDbConnection().getConnection().prepareStatement("UPDATE kingdom_player SET level=? WHERE uuid=?;");
            up.setInt(1,i);
            up.setString(2,uuid.toString());
            up.executeUpdate();
        } catch(Exception e) {
        }
    }

    public int getKingRank() {

        try {
            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM kingdom_player WHERE uuid=?;");
            get.setString(1,uuid.toString());
            ResultSet rs = get.executeQuery();

            if (rs.next()) {
                return rs.getInt("level");
            }
        } catch(Exception e) {e.printStackTrace();}


        return 0;
    }

    public String getKingdom() {

        try {
            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM kingdom_player WHERE uuid=?;");
            get.setString(1,uuid.toString());
            ResultSet rs = get.executeQuery();

            if (rs.next()) {
                return rs.getString("kingdom");
            }
        } catch(Exception e) {e.printStackTrace();}

        return "None";
    }

    public void claim(String name,Chunk chunk) {
        try {
            PreparedStatement ins = Towny.getDbConnection().getConnection().prepareStatement("INSERT INTO chunk_data values(?,?,?);");
            ins.setString(1,name);
            ins.setInt(2,chunk.getX());
            ins.setInt(3,chunk.getZ());
            ins.execute();
            player.sendMessage(ChatColor.YELLOW + "Claimed new chunk");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public boolean ownsTown() {

        try {
            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM town;");
            ResultSet rs = get.executeQuery();

            while (rs.next()) {
                if (rs.getString("uuid").equalsIgnoreCase(uuid.toString())) {
                    return true;
                }
            }
        } catch(Exception e) {
        }

        return false;
    }

    public Location getLocation() {
        return player.getLocation();
    }

    public void claimTown(Location l1, Location l2, Location spawn) {
        try {
            PreparedStatement put = Towny.getDbConnection().getConnection().prepareStatement("INSERT INTO town values(?,?,?,?,?,?,?,?);");

            put.setString(1,uuid.toString());
            put.setInt(1,l1.getBlockX());
            put.setInt(2,l1.getBlockZ());
            put.setInt(3,l2.getBlockX());
            put.setInt(4,l2.getBlockZ());
            put.setInt(5,spawn.getBlockX());
            put.setInt(6,spawn.getBlockY());
            put.setInt(7,spawn.getBlockZ());

            put.execute();

            sendMessage(ChatColor.GREEN + "Town created successfully!");
            playSound(Sound.BLOCK_NOTE_PLING);
        } catch(Exception e) {
            e.printStackTrace();
            sendMessage(ChatColor.RED + "Oh no! We have encountered an error :(");
            playSound(Sound.ENTITY_VILLAGER_NO);
        }
    }

    public boolean isDebug() {
        return getPlayer().getGameMode()== GameMode.CREATIVE;
    }

    public void playSound(Sound sound) {
        player.playSound(getLocation(),sound,1,1);
    }

    public Player getPlayer() {
        return player;
    }
}

package com.azertumc.towny.event;

import com.azertumc.towny.player.TownPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class JoinEvent implements Listener {

    @EventHandler
    public void join(PlayerJoinEvent event) {
        TownPlayer player = new TownPlayer(event.getPlayer().getUniqueId());

        if (event.getPlayer().getLastPlayed() == 0) {
            ItemStack itemStack = new ItemStack(Material.WRITTEN_BOOK);
            BookMeta bookMeta = (BookMeta) itemStack.getItemMeta();

            bookMeta.setTitle(ChatColor.GREEN + "Kingdoms");
            bookMeta.setAuthor(ChatColor.AQUA + "AzertuMC");

            bookMeta.addPage(ChatColor.BOLD + "UPDATES!");
        }

        if (!player.getKingdom().equalsIgnoreCase("None")) {
            player.sendMessage(ChatColor.GREEN + "Town information for " + ChatColor.AQUA + player.getKingdom());
            player.sendMessage(ChatColor.GREEN + "Rating: " + ChatColor.GOLD + "☆☆☆☆☆");
            player.sendMessage(ChatColor.GREEN + "Town Balance: " + ChatColor.RESET + "$0");
        }
    }
}

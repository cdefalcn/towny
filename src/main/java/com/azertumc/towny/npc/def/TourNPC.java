package com.azertumc.towny.npc.def;

import com.azertumc.towny.Towny;
import com.azertumc.towny.npc.NPC;
import com.azertumc.towny.player.TownPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

public class TourNPC extends NPC {

    public TourNPC(Plugin plugin) {
        super(plugin, ChatColor.AQUA + "Tour Guide",new Location(Bukkit.getWorld("bowling"),-568.5, 5, 297.5, -178f, -0f));
    }

    @Override
    public void click(TownPlayer player) {
        player.getPlayer().openInventory(Towny.getFeatured().getInventory());
    }
}

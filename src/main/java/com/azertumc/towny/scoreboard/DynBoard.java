package com.azertumc.towny.scoreboard;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynBoard extends BukkitRunnable implements Listener {

    private Scoreboard scoreboard;
    private Objective objective;
    private String name,temp;
    private Player player;

    private boolean playerCount;
    private int pCountSpot;
    private int seconds;
    private Map<String,Integer> scores;

    public DynBoard(Plugin plugin, String name) {
        seconds=0;
        this.name=name;
        playerCount=false;
        pCountSpot=1;

        scores=new HashMap<>();

        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,this,2L,2L);
    }

    public void setPlayerCount(boolean playerCount) {
        this.playerCount = playerCount;
    }

    public void setpCountSpot(int pCountSpot) {
        this.pCountSpot = pCountSpot;
    }

    public void setVariables(String s) {

        scoreboard= Bukkit.getScoreboardManager().getNewScoreboard();
        objective=scoreboard.registerNewObjective("sidebar","dummy");
        objective.setDisplayName(s);
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.getScore("TEst").setScore(10);

        player.setScoreboard(scoreboard);
    }

    public Objective getObjective() {
        return objective;
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getSeconds() {
        return seconds;
    }


    public Map<String, Integer> getScores() {
        return scores;
    }

    public void addScore(String s, int i) {
        getScores().put(s,i);
    }

    @Override
    public void run() {
        try {
            seconds=seconds+1;
            String name = "";

            if (seconds <= name.length()) {
                seconds=0;
            }

            name= ChatColor.DARK_AQUA + "" + ChatColor.BOLD + this.name.substring(0,seconds) + ChatColor.WHITE + ChatColor.BOLD + this.name.substring(seconds,seconds+1) + ChatColor.AQUA + "" + ChatColor.BOLD + this.name.substring((seconds+1),this.name.length());
            scoreboard= Bukkit.getScoreboardManager().getNewScoreboard();
            objective=scoreboard.registerNewObjective("sidebar","dummy");
            objective.setDisplayName(name);
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);

            if (playerCount) {
                Score players = objective.getScore("Players: " + ChatColor.GREEN + Bukkit.getOnlinePlayers().size());
                players.setScore(pCountSpot);
            }

            Score blank = objective.getScore("  ");
            blank.setScore(-8);

            for (String score : getScores().keySet()) {
                Score s = objective.getScore(score);
                s.setScore(getScores().get(score));
            }

            for (Player player : Bukkit.getOnlinePlayers()) {
                player.setScoreboard(scoreboard);
            }
        } catch(IndexOutOfBoundsException e) {
            seconds=0;
        }

    }
}
package com.azertumc.towny.gui.def;

import com.azertumc.towny.gui.ClickableGUI;
import com.azertumc.towny.item.CItem;
import com.azertumc.towny.player.TownPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.Plugin;

public class Featured extends ClickableGUI {

    public Featured(Plugin plugin) {
        super(plugin,"Featured Kingdoms",27);

        //CItem best = new CItem(ChatColor.RED + "No towns featured", Material.WOOL,(byte) 14);
        //best.addToLore(ChatColor.GRAY + "There are no featured towns at",ChatColor.GRAY + "the moment");

        CItem paper = new CItem(ChatColor.GREEN + "Test Kingdom",Material.PAPER);
        paper.addToLore(
                ChatColor.WHITE + "Owner: " + ChatColor.BLUE + "CodeFalcon",
                ChatColor.WHITE + "Rating: "+ ChatColor.GOLD + "★★★★★",
                "", ChatColor.YELLOW + "Click to teleport");

        CItem help = new CItem(ChatColor.AQUA + "How it works",Material.BOOK);
        help.addToLore(ChatColor.AQUA + "> " + ChatColor.WHITE + "By using " + ChatColor.YELLOW + "/k hype <player>" + ChatColor.WHITE + " you are",
                ChatColor.WHITE + "to vote for the best looking Kingdom.", ChatColor.WHITE + "Kingdoms displayed here will be given rewards.");

        getInventory().setItem(13,paper.getItemStack());
        getInventory().setItem(26,help.getItemStack());
    }

    @Override
    public void click(TownPlayer player, InventoryClickEvent event) {
        event.setCancelled(true);

        if (event.getCurrentItem().getType() == Material.WOOL) {
            player.sendMessage(ChatColor.RED + "No featured towns");
            player.playSound(Sound.ENTITY_VILLAGER_NO);
        }
    }
}

package com.azertumc.towny.town;

import com.azertumc.towny.Towny;
import org.bukkit.Location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

public class Town {

    private UUID uuid;

    private int x1, x2;
    private int z1,z2;

    public Town(UUID uuid) {
        try {
            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM town WHERE uuid=?;");
            ResultSet rs = get.executeQuery();

            if (rs.next()) {
                x1=rs.getInt("x1");
                z1=rs.getInt("z1");
                x2=rs.getInt("x2");
                z1=rs.getInt("z2");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}

package com.azertumc.towny.commands;

import com.azertumc.towny.Towny;
import com.azertumc.towny.flag.Flag;
import com.azertumc.towny.player.TownPlayer;
import com.azertumc.towny.town.TownAPI;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TownyCommands implements CommandExecutor {

    public void sendHelp(TownPlayer player) {
        player.sendMessage(ChatColor.GRAY + "Displaying help for Kingdoms");
        player.sendMessage(ChatColor.YELLOW + "/kingdoms visit <name>");
        player.sendMessage(ChatColor.YELLOW + "/kingdoms hype <name>");
        player.sendMessage(ChatColor.YELLOW + "/kingdoms settings");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]) {

        if (!(sender instanceof Player)) return true;

        TownPlayer player = new TownPlayer(((Player) sender).getUniqueId());

        if (cmd.getName().equalsIgnoreCase("updates")) {

            ItemStack itemStack = new ItemStack(Material.WRITTEN_BOOK);
            BookMeta bookMeta = (BookMeta) itemStack.getItemMeta();

            bookMeta.setTitle(ChatColor.GREEN + "Updates");
            bookMeta.setAuthor("AzertuMC");

            bookMeta.addPage(ChatColor.BOLD + "NOTE FROM DEV\n"
            + ChatColor.RESET +"Our new Kingdoms revamp has been developed around the community building ideas into the experience and making sure there is a unique experience",
                    ChatColor.BOLD + "NEW FEATURES\n" +
                            ChatColor.RESET + "");

            itemStack.setItemMeta(bookMeta);

            player.getPlayer().getInventory().addItem(itemStack);

        }

        if (cmd.getName().equalsIgnoreCase("towny")) {
            if (args.length == 0) {
                sendHelp(player);
                return true;
            }

            if (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
                player.sendMessage(ChatColor.YELLOW + "Displaying help for Towny");
                player.sendMessage(ChatColor.AQUA + "/towny visit <player>");
                player.sendMessage(ChatColor.AQUA + "/towny hype <player>");
                if (player.ownsTown()) {
                    player.sendMessage(ChatColor.AQUA + "/towny management");
                }
                return true;
            }

            if (args[0].equalsIgnoreCase("claim")) {
                if (player.getKingdom().equalsIgnoreCase("None")) {
                    player.sendMessage(ChatColor.RED + "You do not belong to a Kingdom");
                    return true;
                }
                if (!(player.getKingRank() >= 3)) {
                    player.sendMessage(ChatColor.RED + "You have not a high enough rank to claim land");
                    return true;
                }
                player.claim(player.getKingdom(),player.getLocation().getChunk());
            }

            if (args[0].equalsIgnoreCase("testflag")) {
                new Flag(Towny.getInstance(),player.getLocation(),player.getPlayer());
            }

            if (args[0].equalsIgnoreCase("create")) {
                if (!player.getKingdom().equalsIgnoreCase("none")) {
                    player.sendMessage(ChatColor.RED + "You are already apart of a Kingdom");
                    return true;
                }

                if (args.length == 1) {
                    player.sendMessage(ChatColor.GRAY + "You must provide a name");
                    return true;
                }

                String newName = args[1];

                try {
                    PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM kingdom_set WHERE name=?;");
                    get.setString(1,newName);
                    ResultSet rs = get.executeQuery();

                    if (!rs.next()) {
                        PreparedStatement up = Towny.getDbConnection().getConnection().prepareStatement("INSERT INTO kingdom_set values(?,?,?);");
                        up.setString(1,newName);
                        up.setInt(2,0);
                        up.setInt(3,0);
                        up.execute();
                    }

                }catch(Exception e) {
                }

                player.assignKingdom(newName);
            }

            if (args[0].equalsIgnoreCase("disband")) {
                player.disband();
            }

            if (args[0].equalsIgnoreCase("leave")) {
                if (player.getKingRank() == 5) {
                    player.sendMessage(ChatColor.YELLOW + "Use /t disband instead!");
                    return true;
                }
                player.delete();
                player.sendMessage(ChatColor.GREEN + "You have left your Kingdom");
            }

            if (args[0].equalsIgnoreCase("settings")) {
                if (player.getKingdom().equalsIgnoreCase("none")) {
                    player.sendMessage(ChatColor.RED + "You are not apart of a kingdom");
                    return true;
                }

                if (player.getKingRank() == 5) {
                    player.openSettings();
                } else {
                }


            }

            if (args[0].equalsIgnoreCase("chunkinfo")) {
                TownAPI.sendChunkInfo(player);
            }

            if (args[0].equalsIgnoreCase("listall")) {
                if (player.isDebug()) {

                    for (Integer i : TownAPI.getBounds().keySet()) {
                        player.getPlayer().sendMessage(ChatColor.YELLOW + "---- " + ChatColor.AQUA + "TOWNID=" + i + ChatColor.YELLOW + " ----");
                        player.getPlayer().sendMessage(ChatColor.YELLOW + "Bound start: " + TownAPI.getBounds().get(i).getLocation1());
                        player.getPlayer().sendMessage(ChatColor.YELLOW + "Bound end: " + TownAPI.getBounds().get(i).getLocation2());
                    }

                } else {
                    player.sendMessage(ChatColor.RED + "You can not do that :(");
                }
            }

            if (args[0].equalsIgnoreCase("test")) {
                if (player.isDebug()) {
                    player.claimTown(new Location(Bukkit.getWorld("bowling"),-598,4,291),
                            new Location(Bukkit.getWorld("bowling"),-611,4,279),
                            player.getLocation());
                }
            }
        }

        return false;
    }
}

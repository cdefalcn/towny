package com.azertumc.towny.gui;

import com.azertumc.towny.player.TownPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;

public class ClickableGUI implements Listener {

    private Inventory inventory;
    private String name;

    public ClickableGUI(Plugin plugin, String name, int size) {
        this.inventory= Bukkit.createInventory(null,size,name);
        this.name=name;

        Bukkit.getPluginManager().registerEvents(this,plugin);
    }

    public String getName() {
        return name;
    }

    @EventHandler
    public void click(InventoryClickEvent event) {
        try {

            if (!(event.getWhoClicked() instanceof Player)) return;

            if (event.getClickedInventory()==null) return;

            if (event.getCurrentItem() == null) return;

            TownPlayer player = new TownPlayer(((Player) event.getWhoClicked()).getUniqueId());

            if (event.getClickedInventory().getName().equalsIgnoreCase(getName())) {
                click(player,event);
            }


        } catch(Exception e) {
        }
    }

    public void click(TownPlayer player, InventoryClickEvent event){}

    public Inventory getInventory() {
        return inventory;
    }
}

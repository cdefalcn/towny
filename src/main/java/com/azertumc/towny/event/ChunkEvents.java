package com.azertumc.towny.event;

import com.azertumc.towny.Towny;
import com.azertumc.towny.player.TownPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ChunkEvents implements Listener {

    public void move(PlayerMoveEvent event) {
        TownPlayer player = new TownPlayer(event.getPlayer().getUniqueId());
        Location to = event.getTo().clone();
        Location from = event.getFrom().clone();

        Chunk toChunk = to.getChunk();

        try {
            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM chunk_data");
            ResultSet rs = get.executeQuery();

            while (rs.next()) {
                if (toChunk.getX() == rs.getInt("chunkX") && toChunk.getZ() == rs.getInt("chunkZ")) {
                    event.setTo(from);
                    player.sendMessage(ChatColor.RED + "You are not permitted to enter that area!");
                    player.playSound(Sound.BLOCK_NOTE_BASS);
                }
            }
        } catch(Exception e) {
        }
    }

    @EventHandler
    public void blockBreak(BlockPlaceEvent event) {
        TownPlayer player = new TownPlayer(event.getPlayer().getUniqueId());
        Chunk chunk = event.getBlock().getChunk();

        try {
            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM chunk_data");
            ResultSet rs = get.executeQuery();

            while (rs.next()) {
                if (rs.getInt("chunkX") == chunk.getX() && rs.getInt("chunkZ") == chunk.getZ()) {
                    if (!player.getKingdom().equalsIgnoreCase(rs.getString("name"))) {
                        player.sendMessage(ChatColor.RED + "You do not have permission to build in the " + ChatColor.AQUA + rs.getString("name") + ChatColor.RED + " kingdom");
                        player.playSound(Sound.BLOCK_NOTE_BASS);
                        event.setCancelled(true);
                    }
                }
            }
        } catch(Exception e) {
        }
    }

    @EventHandler
    public void blockBreak(BlockBreakEvent event) {
        TownPlayer player = new TownPlayer(event.getPlayer().getUniqueId());
        Chunk chunk = event.getBlock().getChunk();

        try {
            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM chunk_data");
            ResultSet rs = get.executeQuery();

            while (rs.next()) {
                if (rs.getInt("chunkX") == chunk.getX() && rs.getInt("chunkZ") == chunk.getZ()) {
                    if (!player.getKingdom().equalsIgnoreCase(rs.getString("name"))) {
                        if (player.getSetting("build") == 0 && player.getKingRank() < 3) {
                            player.sendMessage(ChatColor.RED + "You do not have permission to build in the " + ChatColor.AQUA + rs.getString("name") + ChatColor.RED + " kingdom");
                            player.playSound(Sound.BLOCK_NOTE_BASS);
                            event.setCancelled(true);
                        }
                    }
                }
            }
        } catch(Exception e) {
        }
    }
}

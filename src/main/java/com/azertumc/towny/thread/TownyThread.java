package com.azertumc.towny.thread;

import com.azertumc.towny.Towny;
import com.azertumc.towny.player.TownPlayer;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TownyThread extends BukkitRunnable {

    private BossBar bossBar;

    public TownyThread(Plugin plugin) {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,this,20L,20L);
        bossBar=Bukkit.createBossBar("loading...", BarColor.PURPLE, BarStyle.SOLID);
    }

    @Override
    public void run() {

        for (Player player : Bukkit.getOnlinePlayers()) {
            Location location = player.getLocation();
            Chunk chunk = location.getChunk();

            try {
                PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM chunk_data");
                ResultSet rs = get.executeQuery();

                while (rs.next()) {
                    if (chunk.getX() == rs.getInt("chunkX") && chunk.getZ() == rs.getInt("chunkZ")) {
                        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(ChatColor.AQUA + "" + ChatColor.BOLD + "Current town: " +
                                ChatColor.YELLOW + ChatColor.BOLD + rs.getString("name")));
                    }
                }
            } catch(Exception e) {
            }
        }

    }
}

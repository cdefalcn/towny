package com.azertumc.towny.npc;

import com.azertumc.towny.player.TownPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.plugin.Plugin;

public class NPC implements Listener {

    private Villager entity;
    private Location location;
    private String name;

    public NPC(Plugin plugin, String name, Location location) {
        this.name=name;
        this.location=location;
        this.entity=location.getWorld().spawn(location,Villager.class);

        this.entity.setCustomName(name);
        this.entity.setCustomNameVisible(true);

        Bukkit.getPluginManager().registerEvents(this,plugin);

        entity.setAI(false);
        entity.setGravity(false);
    }

    @EventHandler
    public void entityDamage(EntityDamageEvent event) {
        if (event.getEntity() == entity) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void click(PlayerInteractEntityEvent event) {
        if (event.getRightClicked() == entity) {
            event.setCancelled(true);
            click(new TownPlayer(event.getPlayer().getUniqueId()));
        }
    }

    public Villager getEntity() {
        return entity;
    }

    public void click(TownPlayer player){}
}

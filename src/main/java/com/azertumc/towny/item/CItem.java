package com.azertumc.towny.item;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class CItem {

    private String name;
    private Material material;

    private List<String> lore;

    private ItemStack itemStack;
    private ItemMeta itemMeta;

    public CItem(String name, Material material) {
        this.name=name;
        this.material=material;
        lore=new ArrayList<>();

        itemStack=new ItemStack(material);
        itemMeta=itemStack.getItemMeta();
    }

    public CItem(String name, Material material, byte data) {
        this.name=name;
        this.material=material;
        lore=new ArrayList<>();

        itemStack=new ItemStack(material,1,data);
        itemMeta=itemStack.getItemMeta();
    }

    public ItemMeta getItemMeta() {
        return itemMeta;
    }

    public void addToLore(String... l) {
        for (String s : l) {
            lore.add(s);
        }
    }

    public ItemStack getItemStack() {
        itemMeta.setLore(lore);
        itemMeta.setDisplayName(name);

        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}

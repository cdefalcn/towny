package com.azertumc.towny.flag;

import com.azertumc.towny.item.CItem;
import org.bukkit.*;
import org.bukkit.block.Banner;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;

public class Flag extends BukkitRunnable {

    private Plugin plugin;
    private Location location;

    private ArmorStand armorStand;
    private Banner banner;

    public Flag(Plugin plugin, Location location, Player player) {
        this.plugin=plugin;
        this.location=location;
        armorStand=location.getWorld().spawn(location,ArmorStand.class);

        armorStand.setCustomName(ChatColor.GREEN + "Friendly Town");
        armorStand.setCustomNameVisible(true);
        armorStand.setVisible(false);
        armorStand.setSmall(true);

        CItem flag = new CItem("", Material.BANNER,(byte) 15);

        armorStand.setHelmet(flag.getItemStack());

        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,this,2L,2L);

    }

    @Override
    public void run() {
        for (int i = 0; i != 3; i++) {
            Location l = location.add(new Random().nextInt(1),new Random().nextInt(1),new Random().nextInt(1));
            location.getWorld().playEffect(l, Effect.COLOURED_DUST,100);
        }
    }
}

package com.azertumc.towny.town;

import com.azertumc.towny.Towny;
import com.azertumc.towny.player.TownPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class TownAPI {

    private static Map<Integer,Bounds> bounds;

    public static void init() {
        bounds=new HashMap<>();
    }

    public static Map<Integer,Bounds> getBounds() {
        return bounds;
    }

    public static void sendChunkInfo(TownPlayer player) {
        Chunk chunk = player.getLocation().getChunk();
        boolean found = false;

        try {
            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM chunk_data;");
            ResultSet rs = get.executeQuery();

            while (rs.next()) {
                if (rs.getInt("chunkX") == chunk.getX() && rs.getInt("chunkZ") == chunk.getZ()) {
                    found=true;

                    player.sendMessage(ChatColor.YELLOW + "Chunk information");
                    player.sendMessage(ChatColor.YELLOW + "Claimed by: " + ChatColor.RESET + rs.getString("name"));
                    player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "✘ " + ChatColor.RED + "Claimed.");
                }
            }
        } catch(Exception e) {
        }

        if (!found) {
            player.sendMessage(ChatColor.YELLOW + "Chunk information");
            player.sendMessage(ChatColor.GREEN + "This chunk can be claimed");
        }
    }

    public static void addAllToMap() {
        int i = 0;
        getBounds().clear();

        try {

            PreparedStatement get = Towny.getDbConnection().getConnection().prepareStatement("SELECT * FROM town;");
            ResultSet rs = get.executeQuery();

            while (rs.next()) {
                Location b = new Location(Bukkit.getWorld("bowling"),rs.getInt("x1"),0,rs.getInt("z1"));
                Location b1 = new Location(Bukkit.getWorld("bowling"),rs.getInt("x2"),1000,rs.getInt("z2"));

                bounds.put(i,new Bounds(b,b1));
                i=i+1;
            }

        } catch(Exception e) {
            e.printStackTrace();
        }

    }
}

package com.azertumc.towny.town;

import org.bukkit.Location;

public class Bounds {

    private Location location1,location2;

    public Bounds(Location location1, Location location2) {
        this.location1=location1;
        this.location2=location2;
    }

    public Location getLocation1() {
        return location1;
    }

    public Location getLocation2() {
        return location2;
    }
}

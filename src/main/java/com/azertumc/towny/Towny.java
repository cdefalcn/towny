package com.azertumc.towny;

import com.azertumc.towny.commands.TownyCommands;
import com.azertumc.towny.event.ChunkEvents;
import com.azertumc.towny.event.JoinEvent;
import com.azertumc.towny.gui.def.Featured;
import com.azertumc.towny.npc.def.HelpNPC;
import com.azertumc.towny.npc.def.TourNPC;
import com.azertumc.towny.scoreboard.DynBoard;
import com.azertumc.towny.sqlite.DBConnection;
import com.azertumc.towny.thread.TownyThread;
import com.azertumc.towny.town.TownAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class Towny extends JavaPlugin {

    private HelpNPC helpNPC;
    private TourNPC tourNPC;
    private TownyCommands townyCommands;
    private DynBoard dynBoard;

    private static Featured featured;
    private static DBConnection dbConnection;

    private static Towny instance;

    @Override
    public void onEnable() {

        instance=this;

        try {
            dbConnection=new DBConnection();
        } catch(Exception e) {
            e.printStackTrace();
        }

        TownAPI.init();
        TownAPI.addAllToMap();

        helpNPC = new HelpNPC(this);
        tourNPC=new TourNPC(this);

        featured=new Featured(this);

        townyCommands=new TownyCommands();

        getCommand("towny").setExecutor(townyCommands);
        getCommand("updates").setExecutor(townyCommands);

        dynBoard=new DynBoard(this,"AZERTUMC");
        dynBoard.setPlayerCount(true);
        dynBoard.setpCountSpot(5);
        dynBoard.addScore(" ",6);
        dynBoard.addScore(ChatColor.WHITE + "Balance: " + ChatColor.GREEN +  "$99999",4 );

        Bukkit.getPluginManager().registerEvents(new ChunkEvents(),this);
        Bukkit.getPluginManager().registerEvents(new JoinEvent(),this);

        new TownyThread(this);
    }

    @Override
    public void onDisable() {
        helpNPC.getEntity().remove();
        tourNPC.getEntity().remove();

        try {

            dbConnection.disconnect();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static Towny getInstance() {
        return instance;
    }

    public static DBConnection getDbConnection() {
        return dbConnection;
    }

    public static Featured getFeatured() {
        return featured;
    }
}
